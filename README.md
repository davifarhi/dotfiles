# Dotfile repo
Author: Davi Farhi  
Contact: <me@davifarhi.ch>  
**[Gitlab page](https://gitlab.com/davifarhi "Gitlab")**

## Usage
Clone this repository inside your main directory creating the following example structure:

	/home/user
	├── .dotfiles
	│   ├── Makefile
	│   └── ...
	└── ...

To install or reinstall all stow dirs run `make install`.  
To remove references to stow dirs run `make clean`.
##### Warning
On linux, dirs named `*_macos` will be ignored and on MacOS dirs named `*_linux` will be ignored.
### Main Dependencies
These are the dependencies required to setup the dotfile repo:
- make
- stow

View also:
[Dependencies](#dependencies)

## Dependencies
##### This list is not complete
- neovim
	- neovim v0.6 or newer
	- [norminette](https://github.com/42School/norminette)
	- [ripgrep](https://github.com/BurntSushi/ripgrep)
	- [fd](https://github.com/sharkdp/fd)
    - [typescript-language-server](https://github.com/typescript-language-server/typescript-language-server)
    - [@prisma/language-server] (https://www.npmjs.com/package/@prisma/language-server)
- i3
	- picom
	- scrot
	- setxkbmap
	- xinput
	- xrandr
	- python3
	- py3status
	- rofi
	- nitrogen

## More information
##### This list is not complete
#### Branches
- main
	>runs on a fedora 34 desktop computer with i3wm and is configured to work with two 1440p monitors, one vertical and the other horizontal.
- laptop
	>runs on a fedora 35 laptop using stock gnome.
	- [higher Xresources dpi](/Xresources/.Xresources "Xresources config")
