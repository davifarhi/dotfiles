ALL_DIRS	= $(wildcard */)
DIRS		:= $(filter-out $(wildcard *_macos/), ${ALL_DIRS})

SYSTEM		= $(shell uname -s | xargs)

ifeq (${SYSTEM}, Linux)
DIRS		:= $(filter-out $(wildcard *_macos/), ${ALL_DIRS})
else ifeq (${SYSTEM}, Darwin)
DIRS		:= $(filter-out $(wildcard *_linux/), ${ALL_DIRS})
endif

install:
			@for dir in ${DIRS} ; do 	\
				stow -D $$dir;			\
				stow $$dir; 			\
				echo "Installing $$dir";\
			done

all:		install

clean:
			@for dir in ${DIRS} ; do	\
				stow -D $$dir;			\
				echo "Removing $$dir";	\
			done

.PHONY:		all clean install
