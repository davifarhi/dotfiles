" project specific vim
set exrc
set secure

set nocompatible

filetype on
filetype plugin on
filetype indent on

set visualbell

syntax on

set rnu
set nu
set ruler

set cursorline
set cursorcolumn

set shiftwidth=4
set tabstop=4

set incsearch

set ignorecase

set hlsearch

set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx,*.o

" keep cursor on the 4th line
" ctrl + e and ctrl + y to move cursor
" zz center cursor
" zt cursor on top
" zb cursor bottom
set so=4

" print all whitespace characters
set list
set listchars=tab:›\ ,eol:¬,space:⋅

au BufRead,BufNewFile *.template,*.tpp set filetype=cpp

lua <<EOF
	require("mylua")
EOF

if (has("termguicolors"))
  set termguicolors
endif

let g:palenight_terminal_italics=1
" Customization on .vim/plugged/material.vim/colors/material.vim
let g:material_theme_style = 'dark'
let g:material_terminal_italics = 1
colorscheme material

let g:lightline = {
  \ 'colorscheme': 'one',
  \ 'component': {
  \   'gitbranch': ' %{fugitive#head()}'
  \ },
  \ 'component_function': {
  \   'current_function': 'LightlineCurrentFunctionVista',
  \ },
  \ }
set laststatus=2

let g:localvimrc_ask=0

let NERDTreeIgnore = ['*.pyc$', '*.o$', '*.pdf$', '*.a$']

" Include path for ale
" let g:ale_c_cc_options = "-Wall -Wextra -Werror -I./includes"
" let g:ale_cpp_cc_options = "-Wall -Wextra -Werror -I/includes"
let g:ale_c_cc_options = "-Wall -Wextra -Werror -I./includes -I./libft/includes -I./minilibx_linux -I./minilibx_macos"
let g:ale_cpp_cc_options = "-Wall -Wextra -Werror -I./includes -I./libft/includes -I./minilibx_linux -I./minilibx_macos"

let g:vimsence_small_text = 'Neovim'
let g:vimsence_small_image = 'neovim'

let g:ale_linters = {
    \ 'python': ['pylint'],
    \ 'vim': ['vint'],
    \ 'cpp': ['clang', 'norminette'],
    \ 'c': ['clang', 'norminette']
\}

" custom setting for clangformat
let g:neoformat_cpp_clangformat = {
    \ 'exe': 'clang-format',
    \ 'args': ['--style="{IndentWidth: 4}"']
\}
let g:neoformat_enabled_cpp = ['clangformat']
let g:neoformat_enabled_c = ['clangformat']

" Highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()

" Preview markdown config
let g:mkdp_auto_start = 1
let g:mkdp_auto_close = 0
let g:mkdp_refresh_slow = 1

function! LightlineCurrentFunctionVista() abort
  let l:method = get(b:, 'vista_nearest_method_or_function', '')
  if l:method != ''
    let l:method = '[' . l:method . ']'
  endif
  return l:method
endfunction
au VimEnter * call vista#RunForNearestMethodOrFunction()

"Switching between source and header file
"https://idie.ru/posts/vim-modern-cpp/#switching-between-source-and-header-file
au BufEnter *.h  let b:fswitchdst = "c,cpp,cc,m"
au BufEnter *.cc let b:fswitchdst = "h,hpp"
au BufEnter *.h let b:fswitchdst = 'c,cpp,m,cc' | let b:fswitchlocs = 'reg:|include.*|src/**|'
nnoremap <silent> <localleader>oo :FSHere<cr>
" Extra hotkeys to open header/source in the split
nnoremap <silent> <localleader>oh :FSSplitLeft<cr>
nnoremap <silent> <localleader>oj :FSSplitBelow<cr>
nnoremap <silent> <localleader>ok :FSSplitAbove<cr>
nnoremap <silent> <localleader>ol :FSSplitRight<cr>

" Remove trailing characters (backslash + rs)
nnoremap <silent> <leader>rs :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>
" Open fzy (control + p)
nnoremap <C-p> :FuzzyOpen<CR>
" Open nerdtree (control + f)
nnoremap <C-f> :NERDTree<CR>
" Open vista (alt + 6)
nnoremap <silent> <A-6> :Vista!!<CR>
" reload vim config (control + alt + r)
nnoremap <A-C-r> :so $MYVIMRC<CR>
" Exit terminal mode with ctrl + e
tnoremap <C-e> <C-\><C-n>

" ===== LuaSnip plugin for snippets L3MON4D3/LuaSnip
" press <Tab> to expand or jump in a snippet. These can also be mapped separately
" via <Plug>luasnip-expand-snippet and <Plug>luasnip-jump-next.
imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>' 
" -1 for jumping backwards.
inoremap <silent> <S-Tab> <cmd>lua require'luasnip'.jump(-1)<Cr>
snoremap <silent> <Tab> <cmd>lua require('luasnip').jump(1)<Cr>
snoremap <silent> <S-Tab> <cmd>lua require('luasnip').jump(-1)<Cr>
" For changing choices in choiceNodes (not strictly necessary for a basic setup).
imap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
smap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
