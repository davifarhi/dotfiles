require('plugins')
require('nv_cmp')
require('nv_lspconfig')
require('nv_trouble')
require('nv_telescope')
require('nv_null-ls')
require('nv_blame')
