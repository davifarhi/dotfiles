require("mason").setup()
require("mason-null-ls").setup({
    ensure_installed = { "stylua", "prettierd", "codespell" },
	automatic_installation = true,
})

local null_ls = require("null-ls")

null_ls.setup({
    sources = {
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.diagnostics.eslint,
        null_ls.builtins.completion.spell,
		null_ls.builtins.formatting.prettierd,
		null_ls.builtins.diagnostics.codespell,
    },
})
