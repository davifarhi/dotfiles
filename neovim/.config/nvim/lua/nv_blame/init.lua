vim.api.nvim_set_var('blamer_enabled', 1)
vim.api.nvim_set_var('blamer_delay', 500)
vim.api.nvim_set_var('blamer_relative_time', 1)

