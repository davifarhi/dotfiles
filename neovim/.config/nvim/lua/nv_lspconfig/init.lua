require("mason").setup {
	ui = {
		icons = {
			package_installed = "✓",
			package_pending = "➜",
			package_uninstalled = "✗",
		}
	}
}
require("mason-lspconfig").setup {
	ensure_installed = { 'clangd', 'bashls', 'lua_ls', 'tsserver', 'eslint', 'prismals', 'volar', 'tailwindcss' },
}

local nvim_lsp = require('lspconfig')

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local servers = { 'clangd', 'bashls', 'lua_ls', 'tsserver', 'eslint', 'prismals', 'volar', 'tailwindcss' }
for _, lsp in ipairs(servers) do
	nvim_lsp[lsp].setup {
		on_attach = on_attach,
		capabilities = capabilities,
	}
end

vim.api.nvim_set_keymap('n', '<Leader>ft', ':Neoformat<CR>', { noremap = true, silent = true })
