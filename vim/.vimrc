" project specific vim
set exrc
set secure

set nocompatible

filetype on
filetype plugin on
filetype indent on

set visualbell

syntax on

set number
set ruler

set cursorline
set cursorcolumn

set shiftwidth=4
set tabstop=4

set incsearch

set ignorecase

set hlsearch

set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" Compile and execute C
nnoremap <f5> :!gcc % && ./a.out

call plug#begin('~/.vim/plugged')
	Plug 'alexandregv/norminette-vim'
	Plug 'embear/vim-localvimrc'
	Plug 'vimsence/vimsence'
	Plug 'dense-analysis/ale'
	Plug 'preservim/nerdtree'
	Plug 'pbondoer/vim-42header'
	Plug 'drewtempelmeyer/palenight.vim'
	Plug 'itchyny/lightline.vim'
	Plug 'https://gitlab.com/davifarhi/darker-materialvim.git'
call plug#end()

if (has("termguicolors"))
  set termguicolors
endif

let g:palenight_terminal_italics=1
" Customization on .vim/plugged/material.vim/colors/material.vim
let g:material_theme_style = 'darker'
let g:material_terminal_italics = 1
colorscheme material

let g:lightline = { 'colorscheme': 'one' }
set laststatus=2

let g:localvimrc_ask=0

" Include path for ale
" let g:ale_c_cc_options = "-Wall -Wextra -Werror -I./includes"
" let g:ale_cpp_cc_options = "-Wall -Wextra -Werror -I/includes"
let g:ale_c_cc_options = "-Wall -Wextra -Werror -I./includes -I./libft/includes"
let g:ale_cpp_cc_options = "-Wall -Wextra -Werror -I/includes -I./libft/includes"
